//
//  PaymentDelegate.swift
//  PointcheckoutSdk
//
//  Created by Abdullah Asendar on 9/17/19.
//  Copyright © 2019 PointCheckout. All rights reserved.
//

import Foundation

public protocol PaymentDelegate {
    func onSuccess(checkoutResponse: CheckoutResponse)
    func onCancel()
    func onError(message: String)
}
