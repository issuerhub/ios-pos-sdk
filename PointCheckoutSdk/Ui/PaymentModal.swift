//
//  PaymentModal.swift
//  PointcheckoutSdk
//
//  Created by Abdullah Asendar on 9/19/19.
//  Copyright © 2019 PointCheckout. All rights reserved.
//

import Foundation


import UIKit

public class PaymentModal: UIView {
    
    private var base64QR: String
    private var events: EventManager
    
    private static let EVENT_CHECK_STATUS: String = "EVN_CEHCK_STATUS"
    private static let EVENT_CANCEL: String = "EVN_CANCEL"

    init(base64QR: String, parent: UIView, paymentStatusHandler: @escaping (()->()), paymentCancelHandler: @escaping (()->())){
        self.base64QR = base64QR
        self.events = EventManager()

        super.init(frame: CGRect.zero)
        self.backgroundColor = UIColor.gray.withAlphaComponent(0.6)
        self.frame = parent.bounds
        self.addSubview(self.container)
        self.container.addSubview(self.pnlMain)
        
        // position the modal on the right of the screen
        let fullScreen = UIDevice.current.userInterfaceIdiom != .pad
        self.container.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.container.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.container.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        self.container.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: (fullScreen ? 1: 0.5)).isActive = true
        
        // set the image width to 75 percent of the container width
        self.imgQrCode.widthAnchor.constraint(equalTo: self.container.widthAnchor, multiplier: 0.75).isActive = true
        self.imgQrCode.heightAnchor.constraint(equalTo: self.imgQrCode.widthAnchor, multiplier: 1).isActive = true
        
        // center the image inside the container
        self.imgQrCode.centerXAnchor.constraint(equalTo: self.container.centerXAnchor).isActive = true
        self.imgQrCode.centerYAnchor.constraint(equalTo: self.container.centerYAnchor).isActive = true
        
        self.events.listenTo(eventName: PaymentModal.EVENT_CHECK_STATUS, action: paymentStatusHandler);
        self.events.listenTo(eventName: PaymentModal.EVENT_CANCEL, action: paymentCancelHandler);

        self.animateIn()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private let container: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .white
        v.layer.cornerRadius = 0
        return v
    }()
    
    private lazy var pnlMain: UIStackView = {
        let stackMain = UIStackView()
        stackMain.axis = .vertical
        stackMain.spacing = 75
        stackMain.translatesAutoresizingMaskIntoConstraints = false
        
        let label = UILabel()
        label.textAlignment = .left
        label.text = "Scan the QR code with PointCheckout app"
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 3
        
        let stackActions = UIStackView()
        stackActions.spacing = 30
        stackActions.translatesAutoresizingMaskIntoConstraints = false
        stackActions.addArrangedSubview(self.btnDismiss)
        stackActions.addArrangedSubview(self.btnCheckStatus)
        self.btnDismiss.widthAnchor.constraint(equalTo: self.btnCheckStatus.widthAnchor).isActive = true
        
        
        stackMain.addArrangedSubview(label)
        stackMain.addArrangedSubview(self.imgQrCode)
        stackMain.addArrangedSubview(stackActions)
        return stackMain
    }()
    
    
    private lazy var imgQrCode: UIImageView = {
        let imageData = Data(base64Encoded: base64QR)
        let image = UIImage(data: imageData!)
        let imageView = UIImageView(image: image!)
        imageView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.width)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    private lazy var btnCheckStatus: UIButton = {
        let button = UIButton()
        button.setTitle("Check payment", for: .normal)
        button.addTarget(self, action: #selector(checkStatusAction), for: .touchUpInside)
        button.backgroundColor = hexStringToUIColor("#25405d")
        button.layer.cornerRadius = 5
        button.clipsToBounds = true
        return button
    }()
    
    private lazy var btnDismiss: UIButton = {
        let button = UIButton()
        button.setTitle("Cancel", for: .normal)
        button.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        button.backgroundColor = hexStringToUIColor("#ee8802")
        button.layer.cornerRadius = 5
        button.clipsToBounds = true
        return button
    }()
    
    private func showSuccess(){
        self.container.backgroundColor = hexStringToUIColor("#5cb85c")
        
        self.pnlMain.subviews.forEach({ $0.removeFromSuperview() })
        
        let label = UILabel()
        label.text = "Payment success"
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 3
        label.font = UIFont.boldSystemFont(ofSize: 30.0)
        label.textColor = .white
        
        self.pnlMain.addArrangedSubview(label)
        
        label.centerXAnchor.constraint(equalTo: self.container.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: self.container.centerYAnchor).isActive = true
    }
    
    private func showFail(){
        showFail(nil)
    }
    private func showFail(_ message: String?){
        self.container.backgroundColor = hexStringToUIColor("#ff0000")
        
        self.pnlMain.subviews.forEach({ $0.removeFromSuperview() })
        
        let label = UILabel()
        label.text = message ?? "Payment failed"
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 3
        label.font = UIFont.boldSystemFont(ofSize: 30.0)
        label.textColor = .white
        
        self.pnlMain.addArrangedSubview(label)
        
        label.centerXAnchor.constraint(equalTo: self.container.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: self.container.centerYAnchor).isActive = true
    }
    
    @objc public func cancel() {
        self.events.trigger(eventName: PaymentModal.EVENT_CANCEL)
        animateOut()
    }
    
    @objc public func dismiss() {
        animateOut()
    }
    
    @objc private func checkStatusAction() {
        self.events.trigger(eventName: PaymentModal.EVENT_CHECK_STATUS)
    }
    
    
    
    private func animateOut() {
        UIView.animate(withDuration: 0.5, delay: 0, animations: {
            self.container.transform = CGAffineTransform(translationX: self.frame.height, y: 0)
            self.alpha = 0
        }) { (complete) in
            if complete {
                self.removeFromSuperview()
            }
        }
        
    }
    
    private func animateIn() {
        self.container.transform = CGAffineTransform(translationX: self.frame.height, y: 0)
        self.alpha = 1
        UIView.animate(withDuration: 0.5, delay: 0, animations: {
            self.container.transform = .identity
            self.alpha = 1
        })
    }
    
    public func getPaymentDelegateWrapper(delegate: PaymentDelegate) -> PcPaymentDelegateWrapper {
        return PcPaymentDelegateWrapper(delegate: delegate, modal: self)
    }
    
    public class PcPaymentDelegateWrapper : PaymentDelegate {
        
        var  delegate: PaymentDelegate
        var modal: PaymentModal
        
        init(delegate: PaymentDelegate, modal: PaymentModal){
            self.delegate = delegate
            self.modal = modal
        }
        
        public func onSuccess(checkoutResponse: CheckoutResponse){
            DispatchQueue.main.async {
                self.modal.showSuccess()
                DispatchQueue.main.asyncAfter(deadline: .now() + 3){
                    self.modal.dismiss()
                }
            }
            delegate.onSuccess(checkoutResponse: checkoutResponse)
        }
        public  func onCancel(){
            DispatchQueue.main.async {
                self.modal.showFail("Payment cancelled")
                DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                    self.modal.dismiss()
                }
            }
            delegate.onCancel()
        }
        public func onError(message: String){
            DispatchQueue.main.async {
                self.modal.showFail()
                DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                    self.modal.dismiss()
                }
            }
            delegate.onError(message: message)
        }
    }
    
    
    // HELPERS
    
    func hexStringToUIColor (_ hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}
