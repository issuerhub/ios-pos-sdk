//
//  PointCheckoutClient.swift
//  PointcheckoutSdk
//
//  Created by Abdullah Asendar on 9/17/19.
//  Copyright © 2019 PointCheckout. All rights reserved.
//

import Foundation
import FirebaseCore
import FirebaseFirestore

public class PointCheckoutClient {
    
    var environment: PointCheckoutEnvironment
    var googleAppID: String
    var apiKey: String
    var apiSecret: String
    var language: String
    var referenceIdDictionary: [String:String]
    private static var firebaseConfigured: Bool?
    
    public init(environment: PointCheckoutEnvironment, apiKey: String, apiSecret: String, googleAppID: String) {
        self.environment = environment
        self.apiKey = apiKey
        self.apiSecret = apiSecret
        self.googleAppID = googleAppID
        self.language = Locale.current.languageCode!
        self.referenceIdDictionary = Dictionary()
    }
    
    public func setLanguage(iso2: String){
        self.language = iso2
    }
    
    public func pay(checkout: CheckoutRequest, paymentDelegate: PaymentDelegate, viewDelegate: ViewDelegate) throws {
        if PointCheckoutUtils.isJailbroken(){
            throw PointCheckoutError.runtimeError("PointCheckout can not run on this device due to security reasons");
        }
        
        try self.postPay(checkout: checkout, checkoutHandler: { checkoutResponse -> Void in
            viewDelegate.onQrCodeGenerated(base64QR: checkoutResponse.base64QR)
            self.setUpFirebase(checkoutResponse: checkoutResponse, paymentDelegate: paymentDelegate)
        }, paymentDelegate: paymentDelegate)
        
    }
    
    public func pay(checkout: CheckoutRequest, paymentDelegate: PaymentDelegate, controller: UIViewController) throws {
        try self.pay(checkout: checkout, paymentDelegate: paymentDelegate, parent: controller.view)
    }
    
    public func pay(checkout: CheckoutRequest, paymentDelegate: PaymentDelegate, parent: UIView) throws {
        if PointCheckoutUtils.isJailbroken(){
            throw PointCheckoutError.runtimeError("PointCheckout can not run on this device due to security reasons");
        }
        
        try self.postPay(checkout: checkout, checkoutHandler: { checkoutResponse -> Void in
            DispatchQueue.main.async{
                let modal = PaymentModal(base64QR: checkoutResponse.base64QR, parent:parent, paymentStatusHandler: {
                    do{
                        try self.paymentStatus(referenceId: checkoutResponse.referenceId, paymentDelegate: paymentDelegate)
                    }catch{
                        print("Error: \(error)")
                    }
                }, paymentCancelHandler: {
                    do{
                        try self.cancel(referenceId: checkoutResponse.referenceId, paymentDelegate: paymentDelegate)
                    }catch{
                        print("Error: \(error)")
                    }
                })
                parent.addSubview(modal)
                self.setUpFirebase(checkoutResponse: checkoutResponse, paymentDelegate: modal.getPaymentDelegateWrapper(delegate: paymentDelegate))
            }
        }, paymentDelegate: paymentDelegate)
    }
    
    public func cancel(referenceId: String, paymentDelegate: PaymentDelegate) throws {
        if PointCheckoutUtils.isJailbroken(){
            throw PointCheckoutError.runtimeError("PointCheckout can not run on this device due to security reasons");
        }
        if(self.referenceIdDictionary[referenceId] == nil){
            throw PointCheckoutError.runtimeError("referenceId \(referenceId) was not found");
        }
        try self.postCancel(referenceId: referenceId, paymentDelegate: paymentDelegate)
    }
    
    public func paymentStatus(referenceId: String, paymentDelegate: PaymentDelegate) throws {
        if PointCheckoutUtils.isJailbroken(){
            throw PointCheckoutError.runtimeError("PointCheckout can not run on this device due to security reasons");
        }
        
        if(self.referenceIdDictionary[referenceId] == nil){
            throw PointCheckoutError.runtimeError("referenceId \(referenceId) was not found");
        }
        
        try self.getPay(referenceId: referenceId, paymentDelegate: paymentDelegate)
    }
    
    public func heartbeat(deviceReference: String) {
        var request = URLRequest(url: URL(string: self.environment.getUrl() + "/api/mer/v1.1/heartbeat")!)
        
        // method
        request.httpMethod = "POST"
        
        // body
        request.httpBody = "{deviceReference: \"\(deviceReference)\"}".data(using: .utf8)
        
        // headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(self.apiKey, forHTTPHeaderField: "Api-Key")
        request.addValue(self.apiSecret, forHTTPHeaderField: "Api-Secret")
        
        URLSession.shared.dataTask(with: request, completionHandler: { data, response, error -> Void in }).resume()
        
    }
    
    
    /********************************
     * HELPERS
     */
    private func postPay(checkout: CheckoutRequest, checkoutHandler: @escaping (CheckoutResponseInternal) -> Void, paymentDelegate: PaymentDelegate) throws {
        
        var request = URLRequest(url: URL(string: self.environment.getUrl() + "/api/mer/v1.1/checkout?qr=true")!)
        
        // method
        request.httpMethod = "POST"
        
        // body
        let jsonEncoder = JSONEncoder()
        let jsonData = try jsonEncoder.encode(checkout)
        let json = String(data: jsonData, encoding: String.Encoding.utf8)
        request.httpBody = json!.data(using: .utf8)
        
        // headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(self.apiKey, forHTTPHeaderField: "Api-Key")
        request.addValue(self.apiSecret, forHTTPHeaderField: "Api-Secret")
        
        URLSession.shared.dataTask(with: request, completionHandler: { data, response, error -> Void in
            do {
                
                if(error != nil) {
                    paymentDelegate.onError(message: String(describing: error))
                }
                
                if(data == nil){
                    return;
                }
                
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                
                if json["error"] != nil {
                    paymentDelegate.onError(message: json["error"] as? String ?? "")
                }else{
                    let result: [String:Any] = json["result"] as! [String:Any]
                    let checkoutResponse: CheckoutResponseInternal = CheckoutResponseInternal(
                        firebaseDatabase: result["firebaseDatabase"] as? String ?? "",
                        firebaseCollection: result["firebaseCollection"] as? String ?? "",
                        firebaseDocument: result["firebaseDocument"] as? String ?? "",
                        referenceId: result["referenceId"] as? String ?? "",
                        base64QR: result["base64QR"] as? String ?? "");
                    
                    checkoutHandler(checkoutResponse)
                    self.referenceIdDictionary[checkout.referenceId] = result["checkoutId"] as? String ?? nil
                }
                
            } catch {
                paymentDelegate.onError(message: "Error: \(error)")
            }
        }).resume()
    }
    
    private func postCancel(referenceId: String, paymentDelegate: PaymentDelegate) throws {
        var request = URLRequest(url: URL(string: self.environment.getUrl() + "/api/mer/v1.1/checkout/\(self.referenceIdDictionary[referenceId]!)/cancel")!)
        
        // method
        request.httpMethod = "POST"
        
        // headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(self.apiKey, forHTTPHeaderField: "Api-Key")
        request.addValue(self.apiSecret, forHTTPHeaderField: "Api-Secret")
        
        URLSession.shared.dataTask(with: request, completionHandler: { data, response, error -> Void in
            
            if(error != nil) {
                paymentDelegate.onError(message: String(describing: error))
            }
            
            if(data == nil){
                return;
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                
                if json["error"] != nil {
                    paymentDelegate.onError(message: json["error"] as? String ?? "")
                }else{
                    paymentDelegate.onCancel()
                }
                
            } catch {
                paymentDelegate.onError(message: "Error: \(error)")
            }
        }).resume()
    }
    
    private func getPay(referenceId: String, paymentDelegate: PaymentDelegate) throws {
        
        if(self.referenceIdDictionary[referenceId] == nil){
            throw PointCheckoutError.runtimeError("referenceId \(referenceId) was not found");
        }
        
        var request = URLRequest(url: URL(string: self.environment.getUrl() + "/api/mer/v1.1/checkout/\(self.referenceIdDictionary[referenceId]!)")!)
        
        // method
        request.httpMethod = "GET"
        
        // headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(self.apiKey, forHTTPHeaderField: "Api-Key")
        request.addValue(self.apiSecret, forHTTPHeaderField: "Api-Secret")
        
        URLSession.shared.dataTask(with: request, completionHandler: { data, response, error -> Void in
            do {
                
                if(error != nil) {
                    paymentDelegate.onError(message: String(describing: error))
                }
                
                if(data == nil){
                    return;
                }
                
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                
                if json["error"] != nil {
                    paymentDelegate.onError(message: json["error"] as? String ?? "")
                }else{
                    let result: [String:Any] = json["result"] as! [String:Any]
                    let checkoutResponse: CheckoutResponse = CheckoutResponse(
                        checkoutId: result["checkoutId"] as? String ?? "",
                        checkoutKey: result["checkoutKey"] as? String ?? "",
                        checkoutDisplayId: result["checkoutDisplayId"] as? String ?? "",
                        referenceId: result["referenceId"] as? String ?? "",
                        deviceReference: result["deviceReference"] as? String ?? "",
                        grandtotal: result["grandtotal"] as? Double ?? 0,
                        currency: result["currency"] as? String ?? "",
                        cash: result["cod"] as? Double ?? 0,
                        status: result["status"] as? String ?? "")
                    
                    if (checkoutResponse.status != "PAID"){
                        paymentDelegate.onError(message: "Payment status: \(checkoutResponse.status)")
                        return
                    }
                    
                    paymentDelegate.onSuccess(checkoutResponse: checkoutResponse)
                }
                
            } catch {
                paymentDelegate.onError(message: "Error: \(error)")
            }
        }).resume()
    }
    
    private func setUpFirebase(checkoutResponse: CheckoutResponseInternal, paymentDelegate: PaymentDelegate){
        
        if (!(PointCheckoutClient.firebaseConfigured ?? false)){
            let options = FirebaseOptions(googleAppID: self.googleAppID, gcmSenderID: "")
            options.projectID = checkoutResponse.firebaseDatabase
            PointCheckoutClient.firebaseConfigured = true
            FirebaseApp.configure(name: "firebaseApp", options: options)
        }
        
        let firebaseApp = FirebaseApp.app(name: "firebaseApp")
        let db = Firestore.firestore(app: firebaseApp!)
        
        db.collection(checkoutResponse.firebaseCollection).document(checkoutResponse.firebaseDocument)
            .addSnapshotListener { documentSnapshot, error in
                guard let document = documentSnapshot else {
                    print("Error fetching document: \(error!)")
                    return
                }
                guard let data = document.data() else {
                    print("Document data was empty.")
                    return
                }
                let status: String = data["status"] as! String
                
                if(status == "PENDING"){
                    return
                }
                
                if(status == "CANCELLED"){
                    paymentDelegate.onCancel()
                    return
                }
                
                if(status != "PAID"){
                    paymentDelegate.onError(message: "Payment status: \(status)")
                    return
                }
                
                do{
                    try self.paymentStatus(referenceId: checkoutResponse.referenceId, paymentDelegate: paymentDelegate)
                }catch{
                    paymentDelegate.onError(message: "Error: \(error)")
                }
        }
    }
    
}
