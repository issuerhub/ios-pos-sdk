//
//  CheckoutRequest.swift
//  PointcheckoutSdk
//
//  Created by Abdullah Asendar on 9/17/19.
//  Copyright © 2019 PointCheckout. All rights reserved.
//

import Foundation

public class CheckoutRequest: Codable {
    
    var referenceId: String
    
    var currency: String
    
    var grandtotal: Double
    
    var subtotal: Double
    
    var shipping: Double?
    
    var handling: Double?
    
    var tax: Double?
    
    var discount: Double?
    
    var deviceReference: String;
    
    var items: Array<CheckoutItem>?
    
    public init(referenceId: String, currency: String, grandtotal: Double, subtotal: Double, deviceReference: String) {
        self.referenceId = referenceId
        self.currency = currency
        self.grandtotal = grandtotal
        self.subtotal = subtotal
        self.deviceReference = deviceReference
    }
}
