//
//  CheckoutItem.swift
//  PointcheckoutSdk
//
//  Created by Abdullah Asendar on 9/17/19.
//  Copyright © 2019 PointCheckout. All rights reserved.
//

import Foundation


public class CheckoutItem: Codable {
    
    var name: String
    
    var sku: String?
    
    var quantity: Double?
    
    var total: Double
    
    public init(name: String, total: Double) {
        self.name = name
        self.total = total
    }
    
}
