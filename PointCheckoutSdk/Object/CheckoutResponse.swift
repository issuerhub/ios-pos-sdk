//
//  CheckoutResponse.swift
//  PointcheckoutSdk
//
//  Created by Abdullah Asendar on 9/18/19.
//  Copyright © 2019 PointCheckout. All rights reserved.
//

import Foundation


public class CheckoutResponse {
    
    public var checkoutId: String
    public var checkoutKey: String
    public var checkoutDisplayId: String
    public var referenceId: String
    public var deviceReference: String
    public var grandtotal: Double
    public var currency: String
    public var cash: Double
    public var status: String
    
    public init(checkoutId: String,
                checkoutKey: String,
                checkoutDisplayId: String,
                referenceId: String,
                deviceReference: String,
                grandtotal: Double,
                currency: String,
                cash: Double,
                status: String){
        self.checkoutId=checkoutId
        self.checkoutKey=checkoutId
        self.checkoutDisplayId=checkoutDisplayId
        self.referenceId=referenceId
        self.deviceReference=deviceReference
        self.grandtotal=grandtotal
        self.currency=currency
        self.cash=cash
        self.status=status
    }
    
}
