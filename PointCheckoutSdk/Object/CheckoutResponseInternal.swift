//
//  CheckoutResponseInternal.swift
//  PointcheckoutSdk
//
//  Created by Abdullah Asendar on 9/17/19.
//  Copyright © 2019 PointCheckout. All rights reserved.
//

import Foundation

public class CheckoutResponseInternal {
    
    public var firebaseDatabase: String
    public var firebaseCollection: String
    public var firebaseDocument: String
    public var referenceId: String
    public var base64QR: String
    
    public init(firebaseDatabase: String, firebaseCollection: String, firebaseDocument: String, referenceId: String, base64QR: String){
        self.firebaseDatabase = firebaseDatabase
        self.firebaseCollection = firebaseCollection
        self.firebaseDocument = firebaseDocument
        self.referenceId = referenceId
        self.base64QR = base64QR
    }
    
}
