Pod::Spec.new do |s|
  s.name         = "PointCheckoutSdk"
  s.version      = "0.1.5"
  s.summary      = "PointCheckout POS Sdk"
  s.description  = "PointCheckout POS Sdk used to submit checkouts"
  s.homepage     = "https://bitbucket.org/issuerhub/ios-pos-sdk"
  
  s.license      = { :type => 'PointCheckout', :file => 'LICENSE' }
  s.author       = { "PointCheckout" => "info@pointcheckout.com" }
  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://bitbucket.org/issuerhub/ios-pos-sdk.git", :tag => "#{s.version}" }
  s.source_files =  "PointCheckoutSdk/**/*.swift"
  
  s.requires_arc = true
  s.swift_version= '5.0'
  
  s.static_framework = true
  
  s.dependency 'Firebase/Core'
  s.dependency 'Firebase/Auth'
  s.dependency 'Firebase/Firestore'

end
